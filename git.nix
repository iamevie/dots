{ pkgs, ... }:
let
	user = "Evie Kalliardos";
	email = "evie@kalliardos.com";
in {
	programs.git = {
		enable = true;
		userName = user;
		userEmail = email;

		extraConfig = {
			credential.credentialStore = "store";
			http.postBuffer = 157286400;
		};
	};
}
