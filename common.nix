{ ... }:

{
	imports = [
		./home.nix
		./shell.nix
		./firefox.nix
		./git.nix
		./nvim.nix
		./steam.nix
		./helix.nix
	];
}
