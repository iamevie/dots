{ config, lib, pkgs, ... }:
let
	mod = "Mod4";
in {	
	home.packages = with pkgs; [
		xclip
		feh
	];

	programs.alacritty.settings.font.size = 12;
	services.picom.enable = true;

	xsession.windowManager.i3 = {
		enable = true;
		config = {
			modifier = mod;

			fonts.names = [ "JetBrainsMono" ];

			keybindings = {
				"${mod}+f" = "fullscreen toggle";
				"${mod}+Return" = "exec alacritty";
				"${mod}+d" = "exec rofi -modi drun -show drun";
				"${mod}+c" = "kill";

				"${mod}+v" = "split h";
				"${mod}+s" = "split v";

				"${mod}+h" = "focus left";
				"${mod}+j" = "focus down";
				"${mod}+k" = "focus up";
				"${mod}+l" = "focus right";

				"${mod}+Shift+h" = "move left";
				"${mod}+Shift+j" = "move down";
				"${mod}+Shift+k" = "move up";
				"${mod}+Shift+l" = "move right";

				"${mod}+0" = "workspace number 0";
				"${mod}+1" = "workspace number 1";
				"${mod}+2" = "workspace number 2";
				"${mod}+3" = "workspace number 3";
				"${mod}+4" = "workspace number 4";
				"${mod}+5" = "workspace number 5";
				"${mod}+6" = "workspace number 6";
				"${mod}+7" = "workspace number 7";
				"${mod}+8" = "workspace number 8";
				"${mod}+9" = "workspace number 9";

				"${mod}+Shift+0" = "move container to workspace number 0";
				"${mod}+Shift+1" = "move container to workspace number 1";
				"${mod}+Shift+2" = "move container to workspace number 2";
				"${mod}+Shift+3" = "move container to workspace number 3";
				"${mod}+Shift+4" = "move container to workspace number 4";
				"${mod}+Shift+5" = "move container to workspace number 5";
				"${mod}+Shift+6" = "move container to workspace number 6";
				"${mod}+Shift+7" = "move container to workspace number 7";
				"${mod}+Shift+8" = "move container to workspace number 8";
				"${mod}+Shift+9" = "move container to workspace number 9";

				"${mod}+m" = "move workspace to output next";
			};

			startup = [
				{
					command = "xrandr --output DP-0 --primary --mode 2560x1440 --rate 165 --output DVI-D-0 --pos 2560x230";
					always = true;
					notification = false;
				}
				{
					command = "feh --bg-scale ~/.background-image";
					always = true;
					notification = false;
				}
				{
					command = "picom";
					always = true;
					notification = false;
				}
			];

			workspaceAutoBackAndForth = true;

			window = {
				titlebar = false;
			};

			gaps = {
				outer = 5;
				inner = 10;
				smartGaps = true;
			};

			floating = {
				titlebar = false;
			};

			# bars.*. = {
			# 	position = "top";
			# };
		};
	};
}
