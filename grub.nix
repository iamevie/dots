{ ... }:

{
	boot.loader = {
		efi.canTouchEfiVariables = true;
		efi.efiSysMountPoint = "/boot";

		grub = {
			device = "nodev";
			enable = true;
			efiSupport = true;
			minegrub-theme = {
				enable = true;
			};
		};
	};
}
