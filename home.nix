{ config, pkgs, inputs, ... }:
let 
	user = "evie";
	version = "23.11";
in {
	home.username = user;
	home.homeDirectory = "/home/${user}";
	home.stateVersion = version;

	programs.home-manager.enable = true;

	home.packages = with pkgs; [
		discord
		signal-desktop
		mattermost-desktop
		zoom-us
		thunderbird
		minecraft
	];

	programs.alacritty = {
		enable = true;
		settings = {
			live_config_reload = true;
			window.dynamic_padding = true;
			window.padding.x = 5;
			window.padding.y = 5;
			window.opacity = 0.8;

			window.resize_increments = true;
			mouse.hide_when_typing = true;
		};
	};


	systemd.user.startServices = "sd-switch";

	home.pointerCursor = {
		gtk.enable = true;
		package = pkgs.bibata-cursors;
		name = "Bibata-Modern-Amber";
		size = 20;
	};

	programs.rofi = {
		enable = true;
	};
}
