{
	description = "...";

	inputs = {
		nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";

		home-manager = {
			url = "github:nix-community/home-manager";
			inputs.nixpkgs.follows = "nixpkgs";
		};

		minegrub-theme.url = "github:Lxtharia/minegrub-theme";

		nixvim = {
			url = "github:nix-community/nixvim";
			inputs.nixpkgs.follows = "nixpkgs";
		};
	};

	outputs = { self, nixpkgs, home-manager, minegrub-theme, nixvim, ... } @ inputs :
	let
		system = "x86_64-linux";
		pkgs = import nixpkgs {
			inherit system;
			config.allowUnfree = true;
		};
		lib = nixpkgs.lib;
		user = "evie";
	in {
		devShells.${system}.default = pkgs.mkShell {

		};
		nixosConfigurations = {
			sunflower = lib.nixosSystem {
				inherit system;
				specialArgs = { inherit inputs; };
				modules = [
					./configuration.nix
					./qwerty.nix
					./greetd.nix
					./xorg.nix
					minegrub-theme.nixosModules.default
					inputs.nixvim.nixosModules.nixvim
					home-manager.nixosModules.home-manager
					{
						home-manager = {
							useGlobalPkgs = true;
							useUserPackages = true;
							sharedModules = [
								inputs.nixvim.homeManagerModules.nixvim
							];
							extraSpecialArgs = { inherit inputs; };
							users.${user}.imports = [
								./i3.nix
								./common.nix
							];
						};
					}
				];
			};
			snowflake = lib.nixosSystem {
				inherit system;
				specialArgs = { inherit inputs; };
				modules = [
					./configuration.nix
					./colemak.nix
					./greetd.nix
					# ./gnome.nix
					minegrub-theme.nixosModules.default
					inputs.nixvim.nixosModules.nixvim
					home-manager.nixosModules.home-manager
					{
						home-manager = {
							useGlobalPkgs = true;
							useUserPackages = true;
							sharedModules = [
								inputs.nixvim.homeManagerModules.nixvim
							];
							extraSpecialArgs = { inherit inputs; };
							users.${user}.imports = [
								./hyprland.nix
								./common.nix
							];
						};
					}
				];
			};
		};
	};
}
