{ self, pkgs, config, ... }:

{
	programs.nixvim = {
		# enable = true;

		options = {
			number = true;
			relativenumber = true;
			shiftwidth = 4;
			tabstop = 4;
			scrolloff = 8;
			undofile = true;
		};

		clipboard.register = "unnamedplus";
		globals.mapleader = " ";

		# plugins = {
		# 	lsp = {
		# 		enable = true;
		# 	};
		# 	telescope.enable = true;
		# 	oil.enable = true;
		# 	treesitter.enable = true;
		# 	luasnip.enable = true;
		# 	lualine.enable = true;

		# 	cmp = {
		# 		enable = true;
		# 		autoEnableSources = true;
		# 	};
		# };

		extraPlugins = with pkgs.vimPlugins; [
		];
	};
}
