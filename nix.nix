{ pkgs, ... }:

{
	nix = {
		package = pkgs.nixFlakes;
		extraOptions = "experimental-features = nix-command flakes";
		settings.auto-optimise-store = true;
		gc.automatic = true;
		gc.dates = "weekly";
		gc.options = "--delete-older-than 7d";
	};

	system.autoUpgrade = {
		enable = true;
		allowReboot = true;
		channel = "https://nixos.org/channels/nixos-unstable";
		dates = "daily";
	};
}
