{ config, lib, pkgs, ... }:
let
	user = "evie";
in {
	imports = [
		/etc/nixos/hardware-configuration.nix
		./grub.nix
		./nix.nix
		./pipewire.nix
		./fonts.nix
		./bluetooth.nix
	];

	networking.networkmanager.enable = true;
	programs.nm-applet.enable = true;
	hardware.opengl.enable = true;

	time.timeZone = "Europe/Berlin";

	programs.zsh.enable = true;

	users = {
		defaultUserShell = pkgs.zsh;
		users.${user} = {
			isNormalUser = true;
			extraGroups = [ "wheel" "sudo" "networkmanager" ];
		};
	};

	environment = {
		systemPackages = with pkgs; [
			neovim
			wget
			git
			rustup
		];
	};

	nixpkgs.config.allowUnfree = true;

	system.stateVersion = "23.11";
}

