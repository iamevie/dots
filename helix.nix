{ self, pkgs, config, ... }:

{
	programs.helix = {
		enable = true;
		settings = {
			theme = "gruvbox_transparent";
			editor.cursor-shape = {
				normal = "block";
				insert = "bar";
				select = "underline";
			};
		};
		languages.language = [];
		themes = {
			gruvbox_transparent = {
				"inherits" = "gruvbox";
				"ui.background" = { };
			};
		};
	};
}
