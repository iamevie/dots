{ pkgs, config, ... }:
let 
	shellAliases = {
		ls = "eza -l --git --git-repos -@ --group-directories-first";
		cat = "bat --plain --theme gruvbox-dark";
		latex = "tectonic -X";
	};

	shellGlobalAliases = {
		rebuild = "sudo nixos-rebuild switch --impure --cores 6 --flake .";
		shell = "nix develop -c zsh";
	};
in
{
	home.packages = with pkgs; [
		eza
		gcc
		coreutils
		btop
		gnumake
		bat
		tectonic
		python3
		jdk
		unzip
		go
	];

	home.sessionPath = [ "$HOME/.cargo/bin" ];

	programs.zsh = {
		enable = true;
		enableCompletion = true;
		enableAutosuggestions = true;
		syntaxHighlighting.enable = true;
		defaultKeymap = "viins";

		inherit shellAliases;
		inherit shellGlobalAliases;
	};

	programs.atuin = {
		enable = true;
		enableZshIntegration = true;
		settings = {
			style = "compact";
			search_mode = "fuzzy";
			filter_mode = "global";
			inline_height = 15;
			show_preview = true;
			exit_mode = "return-query";
			ctrl_n_shortcuts = false;
			common_prefix = [ "sudo" ];
			common_subcommands = [ "cargo" "go" "git" "nvim" ];
			enter_accept = true;
		};
	};

	programs.starship = {
		enable = true;

		settings = {
			format = "[$username on $ssh$hostname in $directory$character](bold)";
			right_format = "[$nix_shell$git_branch$package$time](bold)";
			add_newline = false;

			nix_shell = {
				disabled = false;
				format = "[$symbol]($style)";
				symbol = "  ";
				style = "bold blue";
			};

			hostname = {
				disabled = false;
				trim_at = ".";
				style = "bold yellow";
				format = "[$ssh_symbol$hostname]($style)";
				ssh_only = false;
				ssh_symbol = "(ssh)";
			};

			username = {
				disabled = false;
				show_always = true;
				style_root = "bold red";
				style_user = "bold green";
				format = "[$user]($style)";
			};

			directory = {
				disabled = false;
				truncation_length = 3;
				truncate_to_repo = true;
				home_symbol = "~ ";
				read_only_style = "bold red";
				repo_root_style = "bold purple";
				style = "bold cyan";
				read_only = "";
				format = "[$path]($style)[$read_only]($read_only_style)";
			};

			character = {
				format = "$symbol ";
				success_symbol = "[❯](bold green)";
				error_symbol = "[❯](bold red)";
				vimcmd_symbol = "[v](bold yellow)";
				vimcmd_replace_one_symbol = "[v](bold yellow)";
				vimcmd_replace_symbol = "[v](bold yellow)";
				vimcmd_visual_symbol = "[v](bold yellow)";
			};

			git_branch = {
				format = "[$symbol$branch(:$remote_branch) ]($style)";
				symbol = " ";
				style = "bold purple";
			};

			time = {
				use_12hr = false;
				disabled = false;
				format = "[$time]($style)";
				style = "bold yellow";
			};
		};
	};
}
