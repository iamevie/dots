
{ pkgs, ... }:

{
	fonts.packages = with pkgs; [
		nerdfonts
		inter
	];

	fonts.fontconfig.defaultFonts.monospace = [ "JetBrainsMono" ];
}
