{ pkgs, config, inputs, ... }:

{
	networking.hostName = "sunflower";
	boot.blacklistedKernelModules = ["nouveau"];
	hardware.opengl = {
		enable = true;
		driSupport = true;
		driSupport32Bit = true;
	};

	hardware.nvidia = {
		modesetting.enable = true;
		powerManagement.enable = false;
		powerManagement.finegrained = false;
		open = false;
		nvidiaSettings = true;
		prime.offload.enable = false;
	};

	services.xserver = {
		enable = true;
		videoDrivers = [ "nvidia" ];
		autorun = false;

		libinput = {
			enable = true;
			mouse.accelProfile = "flat";
			touchpad.accelProfile = "flat";
		};

		windowManager.i3.enable = true;

		desktopManager = {
			xterm.enable = false;
			wallpaper.mode = "fill";
		};

		displayManager = {
			startx.enable = true;
			defaultSession = "none+i3";
		};
	};
}
